#include <stdio.h>
#define BLOCK_SIZE 1024
#define NUM_BLOCKS 8
#define PRIMES 1000

__global__ void is_prime(int* nums) {
  int n = threadIdx.x + blockIdx.x * blockDim.x;
  for (int i = 0; i < n; i++) {
    if (nums[i] != 0 && nums[n] % nums[i] == 0) {
      nums[n] = 0;
      break;
    }
  }
}

int main() {
  int *nums;

  cudaMallocManaged(&nums, BLOCK_SIZE * NUM_BLOCKS * sizeof(int));

  for (int i = 0; i < BLOCK_SIZE * NUM_BLOCKS; i++) {
    nums[i] = i + 2;
  }

  is_prime<<<NUM_BLOCKS, BLOCK_SIZE>>>(nums);
  cudaDeviceSynchronize();

  int num_primes = 0;
  for (int i = 0; num_primes < PRIMES && i < BLOCK_SIZE * NUM_BLOCKS; i++) {
    if (nums[i] != 0) {
      printf("%u\n", nums[i]);
      num_primes++;
    }
  }

  cudaFree(nums);

  return 0;
}