(defn isPrime [n i]
  (if (< i 2) true (when (not= 0 (mod n i)) (recur n (dec i)))))
(loop [p 2 count 0]
  (when (< count 1000)
    (if (isPrime p (quot p 2))
      (do (println p)
          (recur (inc p) (inc count)))
      (recur (inc p) count))))
