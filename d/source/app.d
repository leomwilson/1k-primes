import std.stdio;

void main()
{
  uint np = 0;
	for (int i = 2; np < 1000; i++) {
		bool isPrime = true;
		for (int j = 2; j < i; j++) {
			if (i % j == 0) {
				isPrime = false;
				break;
			}
		}
		if (isPrime) {
			np++;
			writeln(i);
		}
	}
}
