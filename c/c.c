#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

void main() {
  int *primes = malloc(1000 * sizeof(int));
  int nprimes = 0;

  for (int i = 2; nprimes < 1000; i++) {
    bool prime = true;
    for (int j = 0; j < nprimes; j++) {
      if (i % primes[j] == 0) {
        prime = false;
        break;
      }
    }
    if (prime) {
      printf("%u\n", i);
      primes[nprimes++] = i;
    }
  }

  free(primes);
}