import kotlin.text.*

fun main() {
  var primes = 0
  var n = 2
  while (primes < 1000) {
    var prime = true
    for (i in 2..(n / 2)) {
      if (n % i == 0) {
        prime = false
        break
      }
    }
    if (prime) {
      println(n)
      primes++
    }
    n++;
  }
}
