func isPrime(p: int): bool =
  result = true
  for i in 2..(p - 1):
    if p mod i == 0:
      return false

var i: int = 2
var n: int = 0
while n < 1000:
  if isPrime(i):
    echo i
    inc n
  inc i

