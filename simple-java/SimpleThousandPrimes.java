public class SimpleThousandPrimes {
  public static void main(String args[]) {
    int numPrimes = 0;
    for (int i = 2; numPrimes < 1000; i++) {
      boolean isDivisible = false;
      for (int j = 2; j <= i / 2; j++) {
        if (i % j == 0) {
          isDivisible = true;
          break;
        }
      }
      if (!isDivisible) {
        System.out.println(i);
        numPrimes++;
      }
    }
  }
}
